import React    from "react";
import template from "./About.jsx";

class About extends React.Component {
  constructor(){
    super();
    this.about=['About our CEO','About our Journey','About our Presence','About our Mission','About our Project','About our Roadmap'];

    }
  render() {
    return template.call(this);
  }
}

export default About;
