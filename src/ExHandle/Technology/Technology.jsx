import "./Technology.css";
import React from "react";

function template() {
  var {name}=this.props;
 
  if(name=="BMW"){
    throw new Error('Invalid Technology Name');
  }
  
  return (
    <div className="technology">
      <h1>I am using {name}</h1>
    </div>
  );
};

export default template;
