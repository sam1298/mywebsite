import "./ExceptionInterceptor.css";
import React from "react";
//set false when there is exception
function template() {
  return (
    <div className="exception-interceptor">
    {this.state.isException ?<h3>May be invalid Technology </h3>: this.props.children}
    </div>
  );
};

export default template;
