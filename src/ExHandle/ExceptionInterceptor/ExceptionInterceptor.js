import React    from "react";
import template from "./ExceptionInterceptor.jsx";
//this cannot be declared in static member
class ExceptionInterceptor extends React.Component {
  constructor()
  {
    super();
    this.state={
      isException:false
    }
  };
  static getDerivedStateFromError(){
    return{
      isException:true
    }
  }
  render() {
    return template.call(this);
  }
}

export default ExceptionInterceptor;
