import "./Team.css";
import React from "react";
import Selectcomp from '../Utilities/Selectcomp/index';
import Tablecomp from '../Utilities/Tablecomp/index';
function template() {
  let management=[
    {
      managers:'Mitesh',
      desig:'VP',
      noofproj:10
    },
    {
    managers:'Bhabesh',
      desig:'Director',
      noofproj:4
    },
    {
      managers:'Mukesh',
        desig:'Senior Manager',
        noofproj:2
      },
      {
        managers:'Sukesh',
          desig:'Program Manager',
          noofproj:1
        },
  ]

  let projects=[
    {
      projname:'Vodafone',
      teamsize:88,
    },
    {
      projname:'AT&T',
      teamsize:300,
    },
  
  ]
  return (
    <div className="team">
      <h1>About our Teams</h1>
      <Selectcomp details={this.teams}/>
    <Tablecomp keys={['managers','desig','noofproj']} header={['Manager','Desig','No of Projects']} data={management}/>
    <br/>
    <Tablecomp keys={['projname','teamsize']} header={['Project Name','Team Size']} data={projects}/>
    <br/>
    <br/>
    
    </div>
  );
};

export default template;
