import React    from "react";
import template from "./Team.jsx";
class Team extends React.Component {
  constructor(){
    super();
    this.teams=['DevTeam','QA Team','Support Team','InfraTeam','NetworkingTeam','IT Teams'];

    }
  render() {
    return template.call(this);
  }
}

export default Team;
