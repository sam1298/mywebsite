import "./Menu.css";
import React,{lazy,Suspense,Component} from "react";
//import Home from '../Home/index';
import {HashRouter,Route} from 'react-router-dom';
//import About from '../About/index';
//import Services from '../Services/index';
//import ContactUs from '../ContactUs/index';
//import Lists from '../Lists/index';
//import Team from '../Team/index';
//In above all components are instantly loaded. Called Aggressive Loading
//It can reduce performance. We can do lazy loading which means component loads on click
//Below we do lazyloading for all components to imporve performance
const Home=lazy(()=>import ('../Home/index'));
const About=lazy(()=>import ('../About/index'));
const Services=lazy(()=>import ('../Services/index'));
const ContactUs=lazy(()=>import ('../ContactUs/index'));
const Lists=lazy(()=>import ('../Lists/index'));
const Team=lazy(()=>import ('../Team/index'));
const Hocomp=lazy(()=>import('../HigherOrderComp'))
//Gives error like suspense fallback.So we need to register suspense fallback
const ExHandle=lazy(()=>import('../ExHandle/index'))
const Quiz=lazy(()=>import('../Quiz/index'))





//react-router-dom returns many things. We need HashRouter
//We do object destructing
/*
example:
function returnArr(){
  return [10,20,30];
}
var [first]=returnArr();
first 
10

Obj destructuring
var obj={ename:'Kamal',esal:'10000'};
var {ename,esal}=obj;
ename
'Kamal'

*/
//Before routing we need to register routing paths

function template() {
  return (
    <div>
    <div className="menu">
      <div className="menu-items">
        <a href="#/home">Home</a>
        <a href="#/about">About</a>
        <a href="#/services">Services</a>
        <a href="#/contactus">ContactUs</a>
        <a href="#/lists">Lists</a>
        <a href="#/teams">Team</a>
        <a href="#/HigherOrderComp">HigherOrderComponent</a>
        <a href="#/Exhandle">ExHandle</a>
        <a href="#/quiz">Quiz</a>
      </div>
      </div>
  
      <HashRouter>
        <Suspense fallback="Loading....">
          
             <Route path="/" exact component={Home}/>
          <Route path="/home" component={Home}/>
          <Route path="/about" component={About}/>
          <Route path="/services"component={Services}/>
          <Route path="/contactus" component={ContactUs}/>
          <Route path="/lists" component={Lists}/>
          <Route path="/teams" component={Team}/>
         <Route path="/HigherOrderComp" component={Hocomp}/>
         <Route path="/ExHandle" component={ExHandle}/>
          <Route path="/quiz" component={Quiz}/>
          </Suspense>
      </HashRouter>
    </div>
  );
};

export default template;
