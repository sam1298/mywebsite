import logo from './logo.svg';
import './App.css';
import Head from './Head/index'
import Menu from './Menu/index'
import Footer from './Footer/index'

function App() {
  return (
    <div className="App">
    <Head/>
    <Menu/>
    <Footer/>

    </div>
  );
}

export default App;
