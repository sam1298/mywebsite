import "./Lists.css";
import React from "react";

function template() {
  //The array will normally be passed from database
   //Array is inside JS inside constructor. Technologies is recognised using this keyword
   //We need to tag with current obj
  let roleNSal=[
    {
      role:'Developer',
      avgSal:2500000
    },
    {
      role:'Lead',
      avgSal:3500000
    },
    {
      role:'Manager',
      avgSal:5000000
    },
    {
      role:'QA',
      avgSal:2000000
    },
  ];
   var questionares=[{
    que:"How was user exp?",
    opt1:"Very good",
    opt2:"good",
    opt3:"average",
    opt4:"needs improvement"
  },
{ que:"How was navigating exp?",
opt1:"Very easy",
opt2:"easy",
opt3:"ok",
opt4:"was not easy at all"
}];


  return (
    
    <div className="lists">
      <h1>List of technologies in product</h1>
      <select>
        {
          this.technologies.map((x,y)=>{
            return <option key={y}>{x}</option>
        })
      }
        </select>
      <h1>List of roles in product</h1>
      <ol>
        {
      this.state.designations.map((value,index)=>{
        return <li key={index}>{value}</li>
      })
    }
    </ol>
    <h1>Questionaries</h1>
    {
      questionares.map((obj,i)=>{
      return <div> <h3 key={i}>Q{i+1}.{obj.que}</h3>
       <div><input type="radio" name={i}/>
         {obj.opt1}
       </div>
       <div><input type="radio" name={i}/>
         {obj.opt2}
       </div>
       <div><input type="radio" name={i}/>
         {obj.opt3}
       </div>
       <div><input type="radio" name={i}/>
         {obj.opt4}
       </div>
       
       </div>
    })
    }
    <br/>
    <h1>Role and Average Salary</h1>
    <table border="2px">
      <thead>
        <tr>
          <th>Role</th>
          <th>AverageSalary</th>
          </tr>
      </thead>
      <tbody>
        {
          roleNSal.map((obj,i)=>{
            return <tr>
              <td>{obj.role}</td>
              <td>{obj.avgSal}</td>
              </tr>
          })
        }

      </tbody>
      </table>
     <br/>
    <br/>
    <h1>Complete info</h1>
    </div>
  );
};

export default template;
