import React    from "react";
import template from "./Lists.jsx";

class Lists extends React.Component {
  constructor(){
    super();
    this.technologies=['HTML','CSS','Angular','Javascript','Bootstrap'];
    this.state={
      designations:['VP','Director','Program Manager','QA Leads','Web Developer']
      
     }
    }
  render() {
    return template.call(this);
  }
}

export default Lists;
