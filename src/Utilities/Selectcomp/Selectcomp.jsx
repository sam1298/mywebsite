import "./Selectcomp.css";
import React from "react";

function template() {
 //const technologies=['html','css','javascript','reactjs'];

  return (
    <div className="selectcomp">
      <h1>Selectcomp</h1>
      <select>
         {
          this.props.details.map((v,i)=>{
            return <option>{v}</option>
          })
         }
      </select>
    </div>
  );
};

export default template;
