import React    from "react";
import template from "./Services.jsx";

class Services extends React.Component {
  constructor(){
    super();
    this.services=['Software Development','ML&AI','SEO','Solutions and Services','Cloud Infrastructure','Networking Solutions'];

    }
  
  render() {
    return template.call(this);
  }
}

export default Services;
