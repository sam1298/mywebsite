import React    from "react";
import operationCount from "../operationCount";
import template from "./ClickCount.jsx";

class ClickCount extends React.Component {
  
  render() {
    return template.call(this);
  }
}

export default operationCount(ClickCount);

