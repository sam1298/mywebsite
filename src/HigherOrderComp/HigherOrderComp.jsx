import "./HigherOrderComp.css";
import React from "react";
import ClickCount from './ClickCount/index';
import HoverCount from './HoverCount/index';
function template() {
  return (
    <div className="higher-order-comp">
      <h1>HigherOrderComp</h1>
    <ClickCount/>
    <HoverCount/>
    </div>
  );
};

export default template;
