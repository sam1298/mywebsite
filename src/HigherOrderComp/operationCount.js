import React from 'react';
const operationCount=(Comp)=>{
class NewOperationCount extends React.Component{
    constructor(){
        super();
        this.state={
         count:0
        }
        this.operationCount=this.operationCount.bind(this);

        }
        operationCount(){
         this.setState({
           count:this.state.count+1
         });
       }
    render(){
       return <Comp operationCount={this.operationCount} count={this.state.count}/>;
    }
}
    return NewOperationCount;
}

export default operationCount;




