import React    from "react";
import template from "./HigherOrderComp.jsx";

class HigherOrderComp extends React.Component {
  render() {
    return template.call(this);
  }
}

export default HigherOrderComp;
